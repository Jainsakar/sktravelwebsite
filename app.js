var express = require('express');
const port = process.env.PORT || 3000;
var app = express();

var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/testapp"); //name of database is testapp

var bodyParser = require('body-parser');
app.use(express.static("public"));
app.set("view engine", "ejs");

//---------------------- SCHEMAS DEFINATION -----------------------

//schema for services offered
var ServiceSchema = new mongoose.Schema({
  serviceName: String,
  serviceDescription: String,
  gridImage : String
});


//schema for storing cars information, this will be used to store any vehicle apart from car for now
var CarSchema = new mongoose.Schema({
  carName : String,
  carDescription : String,
  gridImage : String
});

//-----------------END OF SCHEMA DEFINATION ----------------------

//Service : Allows access to services offered and thus ability to add or remove them from Services Collection
var Service = mongoose.model("Service", ServiceSchema);

var Car = mongoose.model("Car", CarSchema);

//ROUTES DEFINATION START
app.get("/", function(req, res){
  res.render("landing");
});


app.get("/admin", function(req, res){
  res.render("adminPanel");
});


//END OF ALL ROUTES DEFINATION



app.listen(port, function(){
  console.log("Application Started on port "+port);
});
